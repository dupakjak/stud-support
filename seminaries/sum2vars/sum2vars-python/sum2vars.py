#!/usr/bin/python3

var_a = 0x1234
var_b = 0x2222

var_c = var_a + var_b

print('sum %d + %d -> %d'%(var_a, var_b, var_c))

print('sum 0x%x + 0x%x -> 0x%x'%(var_a, var_b, var_c))
